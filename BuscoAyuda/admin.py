from django.contrib import admin

from BuscoAyuda.models import *

# Register your models here.
admin.site.register(Servicio)
admin.site.register(Independiente)
admin.site.register(Comentario)
