__author__ = 'Cristian Huertas'

from unittest import TestCase
from selenium import webdriver
from selenium.webdriver.common.by import By

class FuncionalTest(TestCase):

    def setUp(self):
        self.browser = webdriver.Firefox()


    def tearDown(self):
        self.browser.quit()


    def test_title(self):
        self.browser.get('http://localhost:8000')
        self.assertIn('Busco Ayuda', self.browser.title)

    def test_registro(self):
        self.browser.get('http://localhost:8000')
        link = self.browser.find_element_by_id('id_register')
        link.click()

        self.browser.implicitly_wait(3)

        nombre = self.browser.find_element_by_id('id_first_name')
        nombre.send_keys('Cristian Camilo')

        apellido = self.browser.find_element_by_id('id_last_name')
        apellido.send_keys('Huertas Segura')

        experiencia = self.browser.find_element_by_id('id_experiencia')
        experiencia.send_keys('5')

        self.browser.find_element_by_xpath("//select[@id='id_servicio']/option[text()='Desarrollador Web']").click()
        telefono = self.browser.find_element_by_id('id_telefono')
        telefono.send_keys('6755443')

        correo = self.browser.find_element_by_id('id_email')
        correo.send_keys('cc.huertas@uniandes.edu.co')

        imagen = self.browser.find_element_by_id('id_foto')
        imagen.send_keys('C:\Users\CristianCamilo\Desktop\desarrollador.jpg')

        nombreUsuario = self.browser.find_element_by_id('id_username')
        nombreUsuario.send_keys('camilo8708')

        clave = self.browser.find_element_by_id('id_password')
        clave.send_keys('clave123')

        botonGrabar = self.browser.find_element_by_id('id_grabar')
        botonGrabar.click()
        self.browser.implicitly_wait(3)

        span = self.browser.find_element(By.XPATH, '//span[text()="Cristian Camilo Huertas Segura"]')
        self.assertIn('Cristian Camilo Huertas Segura', span.text)

    def test_verDetalle(self):
        self.browser.get('http://localhost:8000')
        span = self.browser.find_element(By.XPATH, '//span[text()="Cristian Camilo Huertas Segura"]')
        span.click()

        self.browser.implicitly_wait(3)

        h2 = self.browser.find_element(By.XPATH, '//h2[text()="Cristian Camilo Huertas Segura"]')
        self.assertIn('Cristian Camilo Huertas Segura', span.text)
