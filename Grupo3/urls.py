# coding=utf-8
"""Grupo3 URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/1.9/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  url(r'^$', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  url(r'^$', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.conf.urls import url, include
    2. Add a URL to urlpatterns:  url(r'^blog/', include('blog.urls'))
"""
from django.conf.urls import url, patterns
from django.contrib import admin
from django.conf.urls.static import static

from BuscoAyuda import views
from settings import common

urlpatterns = [
    # MAIN URLS
    url(r'^$', views.index, name='index'),
    url(r'^login/*', views.buscoayuda_login, name='login'),
    url(r'^logout/', views.buscoayuda_logout, name='logout'),
    url(r'^register/*', views.buscoayuda_add_independiente, name='register'),
    url(r'^comentarios/add', views.comentario, name='comentario'),
    url(r'^password_change', views.password_change, name='password_change'),
    url(r'^password_change_done', views.password_change_done, name='password_change_done'),
    url(r'^usuario/*', views.usuario, name='usuario'),
    # REST
    url(r'^comentarios/(?P<idIndependiente>[0-9]+)', views.rest_comentarios, name='rest_comentarios'),
    url(r'^independientes/(?P<idIndependiente>[0-9]+)', views.rest_independiente, name='rest_independiente'),
    url(r'^usuarios/(?P<idUser>[0-9]+)', views.rest_user, name='rest_user'),
    url(r'^servicios/(?P<idServicio>[0-9]+)', views.rest_servicio, name='rest_servicio'),
    # ADMIN
    url(r'^admin/', admin.site.urls),
]

urlpatterns += patterns('',
                        url(r'^jsreverse/$', 'django_js_reverse.views.urls_js', name='js_reverse'),
                        )
urlpatterns += static(common.MEDIA_URL, document_root=common.MEDIA_ROOT)
urlpatterns += static(common.STATIC_URL, document_root=common.STATIC_ROOT)
